from PyQt5 import QtCore
import PyQt5.QtCore
from PyQt5.Qt import QVariant
import datetime
import json

class EventTableModel(QtCore.QAbstractTableModel):
    def __init__(self, parent):
        super().__init__(parent)
        self.eventList = []

    def rowCount(self, *args, **kwargs):
        return len(self.eventList)

    def columnCount(self, *args, **kwargs):
        return 5

    def giveList(self, eventList):
        self.beginResetModel()
        self.eventList = eventList
        self.endResetModel()

    def data(self, index, role):
        if(role == PyQt5.QtCore.Qt.DisplayRole):
            return self.getDataForCell(index.column(), index.row())
        return QVariant()

    def getDataForCell(self, col, row):
        event = self.eventList[row]
        return EventTableData.valueForIndex(event, col)

    def headerData(self, col, orientation, role):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return EventTableData.headerName(col)
        return super().headerData(col, orientation, role)

class EventTableData:
    @staticmethod
    def valueForIndex(event, index):
        return [
            EventTableData.formatTime(event['time']),
            event['group']['category']['name'],
            event['group']['name'],
            event['name'],
            event['yes_rsvp_count']
        ][index]

    @staticmethod
    def headerName(index):
        return [
            'Date & Time',
            'Category',
            'Group',
            'Event',
            'RSVP'
        ][index]
        
    @staticmethod
    def formatTime(time):
        time = time/1000
        return datetime.datetime.fromtimestamp(time).strftime('%Y-%m-%d %H:%M:%S')
