from PyQt5.Qt import QTableView, QDesktopServices, QUrl

class EventTableView(QTableView):
    def __init__(self, parent):
        super().__init__(parent)
        self.horizontalHeader().setStretchLastSection(True)
        self.setSortingEnabled(True)
        self.setAlternatingRowColors(True)
        self.doubleClicked.connect(self.onDoubleClick)

    def onDoubleClick(self, modelIndex):
        sourceIndex = self.model().mapToSource(modelIndex)
        event = self.model().sourceModel().eventList[sourceIndex.row()]
        QDesktopServices.openUrl(QUrl(event['link']))
