from PyQt5 import QtCore, Qt
from PyQt5.Qt import QDate, QVariant
import datetime

class FilteredEventTableModel(QtCore.QSortFilterProxyModel):
    def __init__(self, parent):
        super().__init__(parent)
        self.date = QDate.currentDate()
        self.category = ""
        self.minRsvpCount = 0

    def filterAcceptsRow(self, sourceRow, parentIndex):
        event = self.sourceModel().eventList[sourceRow]
        if(not self.validateDate(event)):
            return False
        if(not self.validateCategory(event)):
            return False
        if(not self.validateMinRsvpCount(event)):
            return False
        return True
        
    def validateDate(self, event):
        date = datetime.datetime.fromtimestamp(event['time']/1000).strftime('%Y-%m-%d')
        filterDate = self.date.toString("yyyy-MM-dd")
        if(date == filterDate):
            return True
        else:
            return False
        
    def validateCategory(self, event):
        if(self.category == "" or self.category in event['group']['category']['name']):
            return True
        return False

    def validateMinRsvpCount(self, event):
        if(event['yes_rsvp_count'] >= self.minRsvpCount):
            return True
        return False

    def changeDate(self, date):
        print("FilteredEventTableModel.changeDate: " + self.date.toString("yyyy-MM-dd"))
        self.date = date
        self.invalidate()
        
    def changeCategory(self, category):
        print("FilteredEventTableModel.changeCategory: " + self.category)
        self.category = category
        self.invalidate()
        
    def changeMinRsvpCount(self, minRspvCount):
        self.minRsvpCount = minRspvCount
        self.invalidate()
        
    def headerData(self, section, orientation, role):
        if(role != QtCore.Qt.DisplayRole):
            return super().headerData(section, orientation, role)
        if(orientation == QtCore.Qt.Horizontal):
            return super().headerData(section, orientation, role)
        return section+1

