class FindEventRequest:
    def __init__(self):
        self.request = "/find/events?fields=group_category"

    def __repr__ (self):
        return self.request

    def addRadius(self, len):
        """
        :param len: search radius in miles, x.x format (e.g. 25.0)
        """
        self.request = self.request + "&radius=" + str(len)

    def addStartDate(self, date):
        """
        :param date: Date in yyyy-mm-dd format
        """
        self.request = self.request + "&scroll=since:" + date + "T00:00:00.000-04:00"