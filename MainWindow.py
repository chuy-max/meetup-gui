from PyQt5.Qt import QMainWindow, QIcon, QDate
from UiMainWindow import Ui_MainWindow
from EventTableModel import EventTableModel
from FilteredEventTableModel import FilteredEventTableModel
from MeetupServer import MeetupServer
from FindEventRequest import FindEventRequest

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowIcon(QIcon("logo.png"))
        self.eventServer = MeetupServer()
        self.setupUi()
        self.ui.dateEdit.setDate(QDate.currentDate())
        self.setupTable()
        self.makeConnections()

    def setupUi(self):
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

    def setupTable(self):
        proxyModel = FilteredEventTableModel(self)
        model = EventTableModel(self)
        proxyModel.setSourceModel(model)
        self.ui.tableView.setModel(proxyModel)
        self.generateData()
        self.ui.tableView.resizeColumnsToContents()

    def makeConnections(self):
        self.ui.dateEdit.dateChanged.connect(self.generateData)
        self.ui.radiusSpinBox.valueChanged.connect(self.generateData)
        self.ui.rsvpSpinBox.valueChanged.connect(self.ui.tableView.model().changeMinRsvpCount)
        self.ui.categoryEdit.textChanged.connect(self.ui.tableView.model().changeCategory)
        self.ui.tableView.model().modelReset.connect(self.changeStatusMessage)
        self.ui.tableView.model().modelReset.connect(self.ui.tableView.resizeColumnsToContents)
        self.ui.nextDayButton.clicked.connect(self.changeToNextDay)
        self.ui.prevDayButton.clicked.connect(self.changeToPrevDay)

    def changeToNextDay(self):
        date = self.getFilterDate()
        date = date.addDays(1)
        self.setFilterDate(date)

    def changeToPrevDay(self):
        date = self.getFilterDate()
        date = date.addDays(-1)
        self.setFilterDate(date)

    def generateData(self):
        eventList = self.getEventList()
        self.ui.tableView.model().changeDate(self.getFilterDate())
        self.ui.tableView.model().sourceModel().giveList(eventList)

    def getEventList(self):
        r = FindEventRequest()
        r.addRadius(self.getFilterRadius())
        d = self.getFilterDate().toString("yyyy-MM-dd")
        r.addStartDate(d)
        return self.eventServer.get(r)

    def getFilterDate(self):
        return self.ui.dateEdit.date()

    def setFilterDate(self, newDate):
        self.ui.dateEdit.setDate(newDate)

    def getFilterRadius(self):
        return self.ui.radiusSpinBox.value()

    def changeStatusMessage(self):
        self.ui.statusbar.showMessage("Events: " + str(self.ui.tableView.model().rowCount()))