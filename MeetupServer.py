import requests

class MeetupServer:
    host = "https://api.meetup.com"
    key = "&key=868182a587c5e65b207f5c3f5f516"

    def get(self, request):
        # Get first page of results.
        url = (self.host + str(request) + self.key)
        print(url)
        response = requests.get(url)
        results = response.json()

        # Get second page of results.
        url = response.links['next']['url']
        print(url)
        results = results + requests.get(url).json()

        return results
