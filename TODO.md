Must have:
* Buttons to go to next and previous day.

Nice to have:
* Figure out problems with stdout (buffering)
* Refactor some classes
* Unit tests
